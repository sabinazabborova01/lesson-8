// 
const minutes = document.querySelector('.minutes');
const seconds = document.querySelector('.seconds');
const milliseconds = document.querySelector('.milliseconds');

// Buttons
const start = document.querySelector('.start');
const pause = document.querySelector('.pause');
const reset = document.querySelector('.reset');
const addBtn = document.querySelector('.add');

// 
start.addEventListener('click', () => {
    clearInterval(interval)
    interval = setInterval(startTime, 1000)
})

pause.addEventListener('click', () => {
    clearInterval(interval)
})

reset.addEventListener('click', () => {
    clearInterval(interval)
    clearTimer()
})

addBtn.addEventListener('click', () => {
    clearInterval(interval)
    const results = document.querySelector('.results')
    const block = document.createElement('div')
    block.innerText = `Result: ${minute} : ${second} : ${millisecond}`
    results.append(block)
    clearTimer()
    clearInterval(interval)
    interval = setInterval(startTime, 1000)
    results.append(block)
})

console.log(addBtn);

// Variables
let minute = 00,
    second = 00,
    millisecond = 00,
    interval
   
function startTime() {
    millisecond++
    if(millisecond < 9) {
        milliseconds.innerHTML = "0" + millisecond
    }
    if(millisecond > 9) {
        milliseconds.innerHTML = millisecond
    }
    if(millisecond > 59) {
        second++
        seconds.innerHTML = "0" + second
        millisecond = 0
        milliseconds.innerHTML = "0" + millisecond
    }
    if(second > 59) {
        minute++
        minutes.innerHTML = "0" + minute
        second = 0
        seconds.innerHTML = "0" + second
    }
}

// 
function clearTimer() {
    minute = 00
    second = 00
    millisecond = 00
    seconds.textContent = "00"
    milliseconds.textContent = "00"
    minutes.textContent = "00"
}